const { app, BrowserWindow, ipcMain } = require("electron");
const path = require("path");
const url = require("url");
const { channels } = require("../src/shared/constants");
var ConfigIniParser = require("config-ini-parser").ConfigIniParser;
var fs = require("fs");
const { machineIdSync } = require("node-machine-id");
const { autoUpdater } = require("electron-updater");
require("v8-compile-cache");
let mainWindow;

function createWindow() {
  const startUrl =
    process.env.ELECTRON_START_URL ||
    url.format({
      pathname: path.join(__dirname, "../index.html"),
      protocol: "file:",
      slashes: true,
    });
  mainWindow = new BrowserWindow({ 
    webPreferences: {
 
      preload: path.join(__dirname, "preload.js"),
      minWidth: 1024,
      minHeight: 768,
      //required to integrate node & react
      nodeIntegration: true,
    },
    show: false,
  });
  mainWindow.removeMenu();
  mainWindow.loadURL(startUrl);  
 
  mainWindow.on("closed", function () {
    mainWindow = null;
  });
  mainWindow.once("ready-to-show", () => {
    mainWindow.maximize();   
    //mainWindow.webContents.openDevTools();
    autoUpdater.checkForUpdatesAndNotify();
  });
}

app.on("ready", createWindow);
//needed to add if we are using insecure aws connection
app.commandLine.appendSwitch('ignore-certificate-errors', 'true');

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
app.on("activate", function () {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on(channels.MARVEL_APP_VERSION, (event) => {
  event.sender.send(channels.MARVEL_APP_VERSION, { version: app.getVersion() });
});

//read the ini file
fs.readFile("C:/marvel/ANAAPL.ini", (err, data) => {
  if (data) {
    var delimiter = "\r\n";
    parser = new ConfigIniParser(delimiter);
    parser.parse(data.toString());
    var lsn = parser.get("LSN", "CRT");
    //fetch the UUID
    var uuid = machineIdSync({ original: true });
     //interprocess communication send the lsn,uuid to react components
    ipcMain.on(channels.MARVEL_TERMINAL_INFO, (event) => {
      event.sender.send(channels.MARVEL_TERMINAL_INFO, {
        lsn,
        uuid,
      });
    });
  }
});

autoUpdater.on(channels.MARVEL_UPDATE_AVAILABLE, (event) => {
  mainWindow.webContents.send(channels.MARVEL_UPDATE_AVAILABLE);
});
autoUpdater.on(channels.MARVEL_UPDATE_DOWNLOADED, () => {
  mainWindow.webContents.send(channels.MARVEL_UPDATE_DOWNLOADED);
});

ipcMain.on(channels.MARVEL_RESTART_APP, () => {
  //do a silent installation with restart
  const DO_SILENT_INSTALL = Boolean(true);
  const DO_RESTART_APP = Boolean(true);
  autoUpdater.quitAndInstall(DO_SILENT_INSTALL,DO_RESTART_APP);
});
