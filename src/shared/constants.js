module.exports = {
    channels: {
      MARVEL_TERMINAL_INFO: 'terminal_info',
      MARVEL_APP_VERSION:'app_info',
      MARVEL_UPDATE_AVAILABLE:'update-available',
      MARVEL_UPDATE_DOWNLOADED:'update-downloaded',
      MARVEL_UPDATE_ERROR:'error',
      MARVEL_RESTART_APP:'restart_app' 

    },
  };