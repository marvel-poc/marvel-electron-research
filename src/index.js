
import React from 'react';
import ReactDOM from 'react-dom';
//added for optimizing startup time of app
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primereact/resources/themes/nova-light/theme.css';
import App from './react/App';
import './react/i18n';
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
