import React from 'react';
import splash_bk_marvel from '../assets/img/splash_bk_marvel.png';
import splash_loader from '../assets/img/splash_loader_48-48.gif';

export const Loading = () =>{
    return(
        <div style={{position:'fixed', backgroundColor:'#EEEEEE', width:'100%', height:'100%'}}>
            <div style={{position:'absolute', top:'12em', left:'33%'}}>
                <img style={{height:'70%', width:'70%'}} src={splash_bk_marvel} alt="loading" />
                <img style={{width:'6%', position:'absolute', top:'46%', left:'32%'}} src={splash_loader} alt="loading curl"/>
            </div>
        </div>       
    )
}
    
