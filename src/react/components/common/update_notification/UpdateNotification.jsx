import React, { useEffect, useState } from "react";
import { channels } from "../../../../shared/constants";
const {ipcRenderer} = window;

export default () => {
    const [showNotification, setShowNotification] = useState(false)
    const [showMessage, setShowMessage] = useState(false)
    const [showRestartButton, setShowRestartButton] = useState(false)

  const closeNotification = () => {
      setShowNotification(false)
  };
  const restartApp = () => {
    ipcRenderer.send(channels.MARVEL_RESTART_APP);
  };
 

  const checkforUpdate = () =>{
    ipcRenderer.on(channels.MARVEL_UPDATE_AVAILABLE, () => {
        ipcRenderer.removeAllListeners(channels.MARVEL_UPDATE_AVAILABLE);
        setShowMessage('New update is available for app. Downloading now..');
        setShowNotification(true);
      });
 }

 const updateAndRestart = () => {
    ipcRenderer.on(channels.MARVEL_UPDATE_DOWNLOADED, () => {
        ipcRenderer.removeAllListeners(channels.MARVEL_UPDATE_DOWNLOADED);
        setShowMessage('Update Downloaded. It will be installed on restart. Restart now?');
        setShowNotification(true);
        setShowRestartButton(true);
      });
 }

  useEffect(() => {
     checkforUpdate();     
    updateAndRestart(); 
  }, [showNotification,showMessage])

  return (
  
  <>
    {showNotification && <div  className="update__notification" >
      <p className="update__message" >{showMessage}</p>
      <button className="update__btn update__btn--close" onClick={closeNotification}>
        Close
      </button>
      {showRestartButton && <button className="update__btn update__btn--restart" onClick={restartApp}>
        Restart
      </button>}
    </div>}
    </>
  );
}
