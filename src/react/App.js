import React, { Suspense ,useEffect} from 'react';
import { Provider } from 'react-redux';
import './assets/css/common.css';
import { store } from './helpers/store';
import MarvelApp from './MarvelApp';

import { channels } from '../shared/constants';
import { Loading } from './router/Loading';
const { ipcRenderer } = window; 

 const App=(props) => {
   
  const setupLsnAndUuid = () =>{
    ipcRenderer.send(channels.MARVEL_TERMINAL_INFO);
    ipcRenderer.on(channels.MARVEL_TERMINAL_INFO, (event, arg) => {
      ipcRenderer.removeAllListeners(channels.MARVEL_TERMINAL_INFO);
      const { lsn, uuid } = arg;
      sessionStorage.setItem("lsn",lsn)
      sessionStorage.setItem("uuid",uuid)
    });
   }

   useEffect(() => {
      setupLsnAndUuid();
   },[])
   // fetch the lsn & uuid sent from node (main.js)


  return ( 
    <Suspense fallback={<Loader/>}>
      <Provider store={store}>
        <div className="App">
          <MarvelApp/>
          </div>
      </Provider>
    </Suspense>
  );
}
const Loader = () => (
  <div className="App">
 <Loading/>
  </div>
);
export default  App;